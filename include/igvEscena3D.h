#ifndef __IGVESCENA3D
#define __IGVESCENA3D

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif
#include <cstdlib>
#include <stdio.h>

#include "../include/igvTextura.h"
#include "../include/igvFuenteLuz.h"
#include "Cubo.h"

class igvEscena3D {
	protected:
		Cubo* matriz[24][24][24];
		bool ejes;
		float posX, posY, posZ;
		bool cargada;
		GLint angulox, anguloy;
		igvTextura* textura;
		igvTextura* dirt;
		igvTextura* stone;
		igvTextura* bricks;
		igvTextura* planks;
		igvTextura* diamond;
		igvTextura* iron;
		igvTextura* grass;
		igvTextura* grass_side;
		igvTextura* selected;

	public:
		igvEscena3D();
		void visualizar();
		bool get_ejes() {return ejes;};
		void set_ejes(bool _ejes){ejes = _ejes;};
		void modificarX(float value);
		void modificarY(float value);
		void modificarZ(float value);
		void construir(GLubyte color[3]);
		void romper(GLubyte color[3]);
		void selectTexture(int index);
		igvPunto3D busca_color(GLubyte* color);
		void inline set_angulox(GLint angulo) { angulox = angulo; }
		void inline set_anguloy(GLint angulo) { anguloy = angulo; }
		void pintar_cubos();
		void pintar_VB();
		~igvEscena3D();
};

#endif
