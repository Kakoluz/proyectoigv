#ifndef __IGVTEXTURA
#define __IGVTEXTURA

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif
#include <stdio.h>
#include <vector>

#include "lodepng.h"

class igvTextura {

	protected:
		unsigned int idTextura;
		unsigned int alto, ancho;

	public:
		igvTextura(const char *fichero);
		~igvTextura();

		void aplicar(void);
};

#endif

