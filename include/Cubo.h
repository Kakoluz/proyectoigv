#pragma once
#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif


#include "igvTextura.h"
#include "igvPunto3D.h"

class Cubo
{
private:
	igvPunto3D posicion;
	igvTextura* textura;
	bool drawn;
	float angulox;
	float anguloy;
	GLubyte colores[6][3];

	void pintar_quad(float div_x, float div_y, float div_z);
	GLubyte djb2(GLubyte str);
	
public:
	Cubo(igvTextura& tex, igvPunto3D punt);

	~Cubo();

	inline int getX()
	{
		return posicion[0];
	};
	inline int getY()
	{
		return posicion[1];
	};
	inline int getZ()
	{
		return posicion[2];
	};

	void setangulox(float _angulox) { angulox = _angulox; }
	void setanguloy(float _anguloy) { anguloy = _anguloy; }
	float getangulox() { return angulox; }
	float getanguloy() { return anguloy; }
	void aplicar();
	void applicarVB();
	igvPunto3D getPos() { return posicion; }
	igvTextura* getTex() { return textura; }
	GLubyte* getcolores(int cara);
};