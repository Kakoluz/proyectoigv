#ifndef __IGVINTERFAZ
#define __IGVINTERFAZ

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

#include <string>

#include "igvEscena3D.h"
#include "igvCamara.h"

using namespace std;

typedef enum {
	IGV_VISUALIZAR,
	IGV_SELECCIONAR,
} modoInterfaz;

class igvInterfaz {
	private:
		bool VE;
		int mode;
		bool idle;
		bool girar;
		bool build;
		bool destroy;
		GLint angulox, anguloy;

	protected:
		int ancho_ventana;
		int alto_ventana;
		igvEscena3D escena;
		igvCamara camara;
		modoInterfaz modo;
		int cursorX,cursorY; 
		int objeto_seleccionado;
		bool boton_retenido;
		bool luz;

	public:
		igvInterfaz();
		~igvInterfaz();

		static void set_glutKeyboardFunc(unsigned char key, int x, int y);
		static void set_glutReshapeFunc(int w, int h);
		static void set_glutDisplayFunc();
		static void set_glutIdleFunc();
		static void  set_glutMouseFunc(GLint boton,GLint estado,GLint x,GLint y);
		static void  set_glutMotionFunc(GLint x,GLint y);
		void crear_mundo(void);
		void configura_entorno(int argc, char** argv,
			                     int _ancho_ventana, int _alto_ventana,
			                     int _pos_X, int _pos_Y,
													 string _titulo
													 ); 
		void inicializa_callbacks();
		void inicia_bucle_visualizacion();
		int get_ancho_ventana(){return ancho_ventana;};
		int get_alto_ventana(){return alto_ventana;};
		void set_ancho_ventana(int _ancho_ventana){ancho_ventana = _ancho_ventana;};
		void set_alto_ventana(int _alto_ventana){alto_ventana = _alto_ventana;};
};

#endif
