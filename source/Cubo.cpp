#include "..\include\Cubo.h"
Cubo::Cubo(igvTextura& tex, igvPunto3D punt) : textura(&tex), posicion(punt), drawn(false)
{
	GLubyte posx = djb2((GLubyte)(12 + posicion[0]));
	GLubyte posy = djb2((GLubyte)(12 + posicion[1]));
	GLubyte posz = djb2((GLubyte)(12 + posicion[2]));
	GLubyte colors[6][3] = { { (posx - posy + posz)%255+1,0,0 } //abajo y+
							,{0, (posy - posz + posx)%255+1, 0} //lateral 1 z-
							,{ 0, 0, (posz-posx+posy)%255+1} //lateral 2 z+
							,{ (posx - posz)%255 + 1, (posy - posz)%255 + 1, 0 } //lateral 3 x-
							,{ (posx - posy)%255 + 1, 0, (posz-posy)%255+1 } //lateral 4 x+
							,{ 0, (posy - posx)%255 + 1, (posz -posx)%255+1} }; //arriba y-
	for (int i = 0; i < 6; i++)
		for (int j = 0; j < 3; j++)
			colores[i][j] = colors[i][j];
}

void Cubo::pintar_quad(float tam_x, float tam_y, float tam_z) {
	float ini_x = posicion[0];
	float ini_y = posicion[1];
	float ini_z = posicion[2];

	glDisable(GL_LIGHTING);
	glPushMatrix();
	glColor3ub(colores[0][0], colores[0][1], colores[0][2]);
	glBegin(GL_QUADS);


	glRotatef(angulox, 1, 0, 0);
	glRotatef(anguloy, 0, 1, 0);

	//tapa inferior
	glTexCoord2f(ini_x, 1 -ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] - 0.5, posicion[2] - 0.5); // -.5, -.5, -.5

	glTexCoord2f(ini_x, 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] - 0.5, posicion[2] - 0.5); // .5, -.5, -.5

	glTexCoord2f((ini_x + tam_x), 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] - 0.5, posicion[2] + 0.5); // .5, -.5, .5

	glTexCoord2f((ini_x + tam_x), 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] - 0.5, posicion[2] + 0.5); // -.5, -.5, .5


	glEnd();
	glPopMatrix();

	glPushMatrix();
	glColor3ub(colores[1][0], colores[1][1], colores[1][2]);
	glBegin(GL_QUADS);
	//lateral 1 Z-
	
	glTexCoord2f(ini_x, 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] - 0.5, posicion[2] - 0.5); // -.5, -.5, -.5

	glTexCoord2f(ini_x, 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] - 0.5, posicion[2] - 0.5); // .5, -.5, -.5

	glTexCoord2f((ini_x + tam_x), 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] + 0.5, posicion[2] - 0.5); // .5, -.5, .5

	glTexCoord2f((ini_x + tam_x), 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] + 0.5, posicion[2] - 0.5); // -.5, -.5, .5


	glEnd();
	glPopMatrix();

	glPushMatrix();
	glColor3ub(colores[2][0], colores[2][1], colores[2][2]);
	glBegin(GL_QUADS);
	//lateral 2 Z+

	glTexCoord2f(ini_x, 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] - 0.5, posicion[2] + 0.5); // -.5, -.5, -.5

	glTexCoord2f(ini_x, 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] - 0.5, posicion[2] + 0.5); // .5, -.5, -.5

	glTexCoord2f((ini_x + tam_x), 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] + 0.5, posicion[2] + 0.5); // .5, -.5, .5

	glTexCoord2f((ini_x + tam_x), 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] + 0.5, posicion[2] + 0.5); // -.5, -.5, .5


	glEnd();
	glPopMatrix();

	glPushMatrix();
	glColor3ub(colores[3][0], colores[3][1], colores[3][2]);
	glBegin(GL_QUADS);

	//lateral 3 X-

	glTexCoord2f(ini_x, 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] - 0.5, posicion[2] - 0.5); // -.5, -.5, -.5

	glTexCoord2f(ini_x, 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] - 0.5, posicion[1] - 0.5, posicion[2] + 0.5); // .5, -.5, -.5

	glTexCoord2f((ini_x + tam_x), 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] - 0.5, posicion[1] + 0.5, posicion[2] + 0.5); // .5, -.5, .5

	glTexCoord2f((ini_x + tam_x), 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] + 0.5, posicion[2] - 0.5); // -.5, -.5, .5


	glEnd();
	glPopMatrix();

	glPushMatrix();
	glColor3ub(colores[4][0], colores[4][1], colores[4][2]);
	glBegin(GL_QUADS);

	//lateral 4 X+

	glTexCoord2f(ini_x, 1 - ini_z);
	glVertex3f(posicion[0] + 0.5, posicion[1] - 0.5, posicion[2] - 0.5); // -.5, -.5, -.5

	glTexCoord2f(ini_x, 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] - 0.5, posicion[2] + 0.5); // .5, -.5, -.5

	glTexCoord2f((ini_x + tam_x), 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] + 0.5, posicion[2] + 0.5); // .5, -.5, .5

	glTexCoord2f((ini_x + tam_x), 1 - ini_z);
	glVertex3f(posicion[0] + 0.5, posicion[1] + 0.5, posicion[2] - 0.5); // -.5, -.5, .5

	glEnd();
	glPopMatrix();

	glPushMatrix();
	glColor3ub(colores[5][0], colores[5][1], colores[5][2]);
	glBegin(GL_QUADS);
	//tapa superior
	
	glTexCoord2f(ini_x, 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] + 0.5, posicion[2] - 0.5); // -.5, -.5, -.5

	glTexCoord2f(ini_x, 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] + 0.5, posicion[2] - 0.5); // .5, -.5, -.5

	glTexCoord2f((ini_x + tam_x), 1 - (ini_z + tam_z));
	glVertex3f(posicion[0] + 0.5, posicion[1] + 0.5, posicion[2] + 0.5); // .5, -.5, .5

	glTexCoord2f((ini_x + tam_x), 1 - ini_z);
	glVertex3f(posicion[0] - 0.5, posicion[1] + 0.5, posicion[2] + 0.5); // -.5, -.5, .5


	glEnd();

	glEnable(GL_LIGHTING);
	glPopMatrix();
}

Cubo::~Cubo()
{
}

void Cubo::aplicar()
{
	if (this != nullptr)
	{
		textura->aplicar();
		glPushMatrix();
		pintar_quad(1, 1, 1);
		glPopMatrix();
	}
}

void Cubo::applicarVB()
{
	if (this != nullptr)
	{
		glPushMatrix();
		pintar_quad(1, 1, 1);
		glPopMatrix();
	}
}

GLubyte* Cubo::getcolores(int cara)
{
	if (this != nullptr)
		return colores[cara];
	else
		return nullptr;
}

GLubyte Cubo::djb2(GLubyte str)
{
	unsigned long hash = 5381;
	int c;

	while (c = str++)
		hash = ((hash << 5) + hash) + c;

	return hash % 255;
}

