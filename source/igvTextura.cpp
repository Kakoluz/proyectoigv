#include "..\include\igvTextura.h"

igvTextura::igvTextura(const char *fichero){
	std::vector<unsigned char> out;
	lodepng::decode(out, ancho, alto, fichero);

	size_t u2 = 1; while (u2 < ancho) u2 *= 2;
	size_t v2 = 1; while (v2 < alto) v2 *= 2;
	std::vector<unsigned char> image2(u2 * v2 * 4);
	for (size_t y = 0; y < alto; y++)
		for (size_t x = 0; x < ancho; x++)
			for (size_t c = 0; c < 4; c++) {
				image2[4 * u2 * y + 4 * x + c] = out[4 * ancho * y + 4 * x + c];
			}
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &idTextura);
	glBindTexture(GL_TEXTURE_2D, idTextura);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, u2, v2, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image2[0]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}

void igvTextura::aplicar(void) {
	if (this != nullptr)
		glBindTexture(GL_TEXTURE_2D, idTextura);
}

igvTextura::~igvTextura() {
  glDeleteTextures(1, &idTextura); 
}
