#include <cstdlib>
#include <stdio.h>

#include "..\include\igvInterfaz.h"

using namespace std;

extern igvInterfaz interfaz;


igvInterfaz::igvInterfaz () {
	modo = IGV_VISUALIZAR;
	objeto_seleccionado = -1;
	boton_retenido = false;
	luz = true;
	VE = false;
	idle = true;
	girar = false;
	build = false;
	destroy = false;
	angulox = 0;
	anguloy = 0;
}

igvInterfaz::~igvInterfaz () {}

void igvInterfaz::crear_mundo(void)
{
	interfaz.camara.set(IGV_PARALELA, igvPunto3D(6.0, 4.0, 8), igvPunto3D(0, 0, 0), igvPunto3D(0, 1.0, 0),
		-1 * 3, 1 * 3, -1 * 3, 1 * 3, -1 * 3, 200);
}
void igvInterfaz::configura_entorno(int argc, char** argv,
			                              int _ancho_ventana, int _alto_ventana,
			                              int _pos_X, int _pos_Y,
													          string _titulo){
	// inicialización de las variables de la interfaz																	
	ancho_ventana = _ancho_ventana;
	alto_ventana = _alto_ventana;

	// inicialización de la ventana de visualización
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(_ancho_ventana,_alto_ventana);
	glutInitWindowPosition(_pos_X,_pos_Y);
	glutCreateWindow(_titulo.c_str());

	glEnable(GL_DEPTH_TEST); // activa el ocultamiento de superficies por z-buffer
	glClearColor(1.0,1.0,1.0,0.0); // establece el color de fondo de la ventana

	glEnable(GL_LIGHTING); // activa la iluminacion de la escena
	glEnable(GL_NORMALIZE); // normaliza los vectores normales para calculo iluminacion

	crear_mundo(); // crea el mundo a visualizar en la ventana
}

void igvInterfaz::inicia_bucle_visualizacion() {
	glutMainLoop();
}

void igvInterfaz::set_glutKeyboardFunc(unsigned char key, int x, int y) {
	switch (key) {
		case 'a':
			interfaz.girar = !(interfaz.girar);
			break;
		case 'v': 
			interfaz.mode++;
			interfaz.mode = interfaz.mode % 4;
			switch (interfaz.mode)
			{
			case(0):
				interfaz.camara.set(igvPunto3D(0, 5, 0), igvPunto3D(0, 0, 0), igvPunto3D(1, 0, 0));
				break;
			case(1):
				interfaz.camara.set(igvPunto3D(0, 0, 5), igvPunto3D(0, 0, 0), igvPunto3D(0, 1.0, 0));
				break;
			case(2):
				interfaz.camara.set(igvPunto3D(5, 0, 0), igvPunto3D(0, 0, 0), igvPunto3D(0, 1.0, 0));
				break;
			case(3):
				interfaz.camara.set(igvPunto3D(3.0, 2.0, 4), igvPunto3D(0, 0, 0), igvPunto3D(0, 1.0, 0));
				break;
			}
			break;
		case 'l':
			if (interfaz.luz)
			{
				glDisable(GL_LIGHTING);
				glDisable(GL_TEXTURE_2D);	
				interfaz.luz = false;
			}
			else
			{
				glEnable(GL_LIGHTING);
				glEnable(GL_TEXTURE_2D);
				interfaz.luz = true;
			}
			break;
		case'1':
			interfaz.escena.selectTexture(1);
			break;
		case'2':
			interfaz.escena.selectTexture(2);
			break;
		case'3':
			interfaz.escena.selectTexture(3);
			break;
		case'4':
			interfaz.escena.selectTexture(4);
			break;
		case'5':
			interfaz.escena.selectTexture(5);
			break;
		case'6':
			interfaz.escena.selectTexture(6);
			break;
		case'7':
			interfaz.escena.selectTexture(7);
			break;
		case'8':
			interfaz.escena.selectTexture(8);
			break;
		case'9':
			interfaz.escena.selectTexture(9);
			break;
		case 27: // tecla de escape para SALIR
			exit(1);
		break;
  }
	glutPostRedisplay();
}

void igvInterfaz::set_glutReshapeFunc(int w, int h) {

	interfaz.set_ancho_ventana(w);
	interfaz.set_alto_ventana(h);

	interfaz.camara.aplicar();
}

void igvInterfaz::set_glutDisplayFunc() {
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // borra la ventana y el z-buffer
	// se establece el viewport
	glViewport(0, 0, interfaz.get_ancho_ventana(), interfaz.get_alto_ventana());

	if (interfaz.modo == IGV_SELECCIONAR) {
        glDisable(GL_LIGHTING);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_CULL_FACE);

		interfaz.camara.aplicar();
		interfaz.escena.pintar_VB();
		GLubyte pixel[3];
		glReadPixels(interfaz.cursorX, interfaz.alto_ventana - interfaz.cursorY, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
		if(interfaz.build)
			interfaz.escena.construir(pixel);
		if (interfaz.destroy && !interfaz.girar)
			interfaz.escena.romper(pixel);

		interfaz.modo = IGV_VISUALIZAR;
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_LIGHTING);
		glutPostRedisplay();
	} else {
		interfaz.camara.aplicar();
		interfaz.escena.visualizar();	
		glutSwapBuffers();
	}
}

void igvInterfaz::set_glutMouseFunc(GLint boton, GLint estado, GLint x, GLint y) {
	if (boton == 2)
	{
		interfaz.idle = false;
		if (estado == GLUT_DOWN)
		{
			interfaz.build = true;
			interfaz.modo = IGV_SELECCIONAR;
		}
		if (estado == GLUT_UP)
		{
			interfaz.build = false;
		}
		interfaz.cursorX = x;
		interfaz.cursorY = y;
	}
	if (boton == 0)
	{
		interfaz.idle = false;
		if (estado == GLUT_UP)
		{
			interfaz.boton_retenido = false;
			interfaz.destroy = false;
		}
		if (estado == GLUT_DOWN)
		{
			interfaz.boton_retenido = true;
			interfaz.destroy = true;
			interfaz.modo = IGV_SELECCIONAR;
		}
		interfaz.cursorX = x;
		interfaz.cursorY = y;
	}
	glutPostRedisplay();
}

void igvInterfaz::set_glutMotionFunc(GLint x,GLint y) {
	if (interfaz.boton_retenido && interfaz.girar)
	{
		interfaz.angulox = interfaz.cursorX + x;
		interfaz.escena.set_angulox(interfaz.angulox);
		interfaz.anguloy = interfaz.alto_ventana - interfaz.cursorY - y;
		interfaz.escena.set_anguloy(interfaz.anguloy);
		interfaz.cursorX = x;
		interfaz.cursorY = y;
	}
	glutPostRedisplay();
}

void igvInterfaz::set_glutIdleFunc() {
	if (interfaz.idle)
	{
		Sleep(50);

		glutPostRedisplay();
	}
}

void igvInterfaz::inicializa_callbacks() {
	glutKeyboardFunc(set_glutKeyboardFunc);
	glutReshapeFunc(set_glutReshapeFunc);
	glutDisplayFunc(set_glutDisplayFunc);

    glutMouseFunc(set_glutMouseFunc);
    glutMotionFunc(set_glutMotionFunc);
	glutIdleFunc(set_glutIdleFunc);
}

