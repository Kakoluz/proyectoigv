#include "../include/igvEscena3D.h"



igvEscena3D::igvEscena3D () {
	ejes = true;

	posX = 3;
	posY = 1;
	posZ = 1;
	
	angulox = 0;
	anguloy = 0;

}

igvEscena3D::~igvEscena3D() {
	delete textura;
	delete dirt;
	delete stone;
	delete bricks;
	delete planks;
	delete diamond;
	delete iron;
	delete grass;
	delete grass_side;
	delete selected;
	for (int i = 0; i < 24; i++)
	{
		for (int j = 0; j < 24; j++)
		{
			for (int k = 0; k < 24; k++)
			{
				delete matriz[i][j][k];
			}
			delete[] matriz[i][j];
		}
		delete[] matriz[i];
	}
	delete[] matriz;
}

void pintar_ejes(void) {
  GLfloat rojo[]={1,0,0,1.0};
  GLfloat verde[]={0,1,0,1.0};
  GLfloat azul[]={0,0,1,1.0};

  glMaterialfv(GL_FRONT,GL_EMISSION,rojo);
	glBegin(GL_LINES);
		glVertex3f(1000,0,0);
		glVertex3f(-1000,0,0);
	glEnd();

  glMaterialfv(GL_FRONT,GL_EMISSION,verde);
	glBegin(GL_LINES);
		glVertex3f(0,1000,0);
		glVertex3f(0,-1000,0);
	glEnd();

  glMaterialfv(GL_FRONT,GL_EMISSION,azul);
	glBegin(GL_LINES);
		glVertex3f(0,0,1000);
		glVertex3f(0,0,-1000);
	glEnd();
}


void igvEscena3D::visualizar(void) {
	glPushMatrix();

	igvFuenteLuz luz1 = igvFuenteLuz(GL_LIGHT0, { posX,posY,posZ }, { 0,0,0,1 }, { 1,1,1,1 }, { 1,1,1,1 }, 1, 0, 0);
	luz1.encender();
	luz1.aplicar();
	  
	glRotatef(angulox, 0, 1, 0);
	glRotatef(anguloy, 1, 0, 0);

	if (!cargada)
	{
		dirt = new igvTextura("resources\\textures\\dirt.png");
		stone = new igvTextura("resources\\textures\\stone.png");
		bricks = new igvTextura("resources\\textures\\bricks.png");
		planks = new igvTextura("resources\\textures\\oak_planks.png");
		diamond = new igvTextura("resources\\textures\\diamond_ore.png");
		iron = new igvTextura("resources\\textures\\iron_ore.png");
		grass = new igvTextura("resources\\textures\\grass_block_top.png");
		grass_side = new igvTextura("resources\\textures\\grass_block_side.png");
		textura = new igvTextura("resources\\textures\\default.png");
		selected = textura;

		//Inicialización de los bloques
		//Cubo* matriz[24][24][24]; //la matriz es de tamaño 12 * 2 para poder representar posiciones negativas sin necesitar índices negativos
		matriz[12][12][12] = new Cubo(*textura, igvPunto3D(0, 0, 0)); //0, 0, 0
		matriz[11][12][12] = new Cubo(*dirt, igvPunto3D(-1, 0, 0));
		matriz[11][12][13] = new Cubo(*dirt, igvPunto3D(-1, 0, 1));
		matriz[11][12][11] = new Cubo(*dirt, igvPunto3D(-1, 0, -1));
		matriz[12][12][11] = new Cubo(*stone, igvPunto3D(0, 0, -1));
		matriz[12][12][13] = new Cubo(*iron, igvPunto3D(0, 0, 1));
		matriz[13][12][12] = new Cubo(*diamond, igvPunto3D(1, 0, 0));
		matriz[13][12][11] = new Cubo(*bricks, igvPunto3D(1, 0, -1));
		matriz[13][12][13] = new Cubo(*planks, igvPunto3D(1, 0, 1));

		cargada = true;
	}

	pintar_cubos();

	glPopMatrix ();
}



void igvEscena3D::modificarX(float value)
{
	posX += value;
}

void igvEscena3D::modificarY(float value)
{
	posY += value;
}

void igvEscena3D::modificarZ(float value)
{
	posZ += value;
}

void igvEscena3D::construir(GLubyte color[3]){
		igvPunto3D punto = busca_color(color);
		if (color[0] != 0 && color[1] == 0 && color[2] == 0) {//tapa superior
			punto[1] -= 1;
		}
		else if (color[0] == 0 && color[1] != 0 && color[2] == 0) {//lateral 1
			punto[2] += 1;
		}
		else if (color[0] == 0 && color[1] == 0 && color[2] != 0) { //lateral 2
			punto[2] -= 1;
		}
		else if (color[0] != 0 && color[1] != 0 && color[2] == 0) { //lateral 3
			punto[0] += 1;
		}
		else if (color[0] != 0 && color[1] == 0 && color[2] != 0) { //lateral 4
			punto[0] -= 1;
		}
		else if (color[0] == 0 && color[1] != 0 && color[2] != 0) { //tapa inferior
			punto[1] += 1;
		}
		matriz[(int)punto[0] + 12][(int) punto[1] + 12][(int) punto[2] + 12] = new Cubo(*selected, punto);
}

void igvEscena3D::romper(GLubyte color[3]) {
	igvPunto3D punto = busca_color(color);
	delete matriz[(int)punto[0] + 12][(int)punto[1] + 12][(int)punto[2] + 12];
	matriz[(int)punto[0] + 12][(int)punto[1] + 12][(int)punto[2] + 12] = nullptr;
}

void igvEscena3D::selectTexture(int index)
{
	switch (index)
	{
	case 1:
		selected = dirt;
		break;
	case 2:
		selected = stone;
		break;
	case 3:
		selected = bricks;
		break;
	case 4:
		selected = planks;
		break;
	case 5:
		selected = iron;
		break;
	case 6:
		selected = diamond;
		break;
	default:
		selected = textura;
		break;
	}

}

void igvEscena3D::pintar_cubos()
{
	for (int i = 0; i < 24; i++)
		for (int j = 0; j < 24; j++)
			for (int y = 0; y < 24; y++)
				if (matriz[i][j][y] != nullptr)
					matriz[i][j][y]->aplicar();
}

void igvEscena3D::pintar_VB()
{
	for (int i = 0; i < 24; i++)
		for (int j = 0; j < 24; j++)
			for (int y = 0; y < 24; y++)
				matriz[i][j][y]->applicarVB();
}

igvPunto3D igvEscena3D::busca_color(GLubyte* color){
	bool enc = false;
	igvPunto3D punto;
	GLubyte ubColor[] = { color[0], color[1], color[2] };
	for(int i=0; i < 24 && !enc; ++i)
		for( int j=0; j < 24 && !enc; ++j)
			for (int k = 0; k < 24 && !enc; ++k)
				if (matriz[i][j][k] != nullptr)
					for (int z = 0; z < 6; ++z)
					{
						GLubyte gottenColor[] = { matriz[i][j][k]->getcolores(z)[0], matriz[i][j][k]->getcolores(z)[1], matriz[i][j][k]->getcolores(z)[2] };
						if (gottenColor[0] == ubColor[0] && gottenColor[1] == ubColor[1] && gottenColor[2] == ubColor[2])
						{
							enc = true;
							punto = matriz[i][j][k]->getPos();
						}
					}
	if (enc)
		return punto;
	return (igvPunto3D(0,0,0));
}